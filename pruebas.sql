--SELECT last_name, (SYSDATE - hire_date) / 7  -- DA EL NUMERO DE SEMANAS QUE HA TRABJADO DESDE QUE FUE CONTRATADO
--FROM employees;


--******************************************************************************

--SELECT employee_id, (end_date - start_date)/365 AS "Tenure in last job" FROM job_history;

---*****************************************************************************
--SELECT last_name, hire_date FROM employees
--WHERE MONTHS_BETWEEN (SYSDATE, hire_date) > 50;  --TE DA LOS MESES QUE HAY ENTRE UNA FECHA Y OTRA Y MUESTRA LOS QUE SON MAYORES A 50

---*****************************************************************************
--SELECT ADD_MONTHS (SYSDATE, 12) AS "Next Year" FROM dual;  
--AGREGA CIERTO NUMERO DE MESES A LA FECHA ACTUAL

---*****************************************************************************

--SELECT NEXT_DAY (SYSDATE, 'S�bado') AS "Next Saturday" FROM dual; --Te da la fecha del d�a proximo que se se�ala

---*****************************************************************************

--SELECT LAST_DAY (SYSDATE) AS "End of the Month" FROM dual; Da el �ltimo d�a del mes de la fecha actual


---****************************************************************************

--SELECT hire_date, ROUND(hire_date, 'Month') FROM employees
--WHERE department_id = 50;      --Con el ROUND en este caso, lo que hace es que si la fecha est� dentro de los primeros 15 d�as, redondea la fecha al primer d�a de ese mismo mes, 
                               --De lo contrario, pasando los 15 d�as, redondea la fecha al primer d�a del SIGUIENTE mes.
                               --Lo mismo hace con YEAR, estando dentro de los primeros 6 meses, la fecha la pasa para el primer d�a del primer mes de ese a�o, de lo contrario, pasa la fecha al primer d�a del primer mes del siguiente a�o.
---****************************************************************************

--SELECT hire_date, TRUNC(hire_date, 'Month') FROM employees
--WHERE department_id = 50;     --TRUNC lo que hace es (en 'Month') regresa la fecha con el primer d�a del mes de la fecha original, en el caso de 'Year' regresa la fecha del primer d�a del primer mes del a�o de la fecha original.

---****************************************************************************

--SELECT employee_id, hire_date,
--ROUND(MONTHS_BETWEEN(SYSDATE, hire_date)) AS TENURE,  --Cuenta lo meses ue hay entre la fecha actual y la fecha de contrataci�n, y ese n�mero lo redondea.
--ADD_MONTHS (hire_date, 6) AS REVIEW, -- Agrega 6 meses a la fecha de contrataci�n
--NEXT_DAY(hire_date, 'VIERNES'), LAST_DAY(hire_date)  --Da la fecha de viernes proximo y la fecha del �ltimo mes a la fecha de contrataci�n.
--FROM employees
--WHERE MONTHS_BETWEEN (SYSDATE, hire_date) > 36; --unicamente mostrar� los empleados que la diferencia de meses entre la fecha de contrataci�n y la fecha actual sea mayor a 36


---****************************************************************************
--SELECT TO_CHAR(hire_date, 'fmDay ddthsp Mon, YYYY') --cambia el formato de la fecha y la pasa a tipo char.
--FROM employees;


--SELECT SYSDATE, TO_CHAR(SYSDATE,  'fmDay ddthsp Mon, YYYY')  --podemos cambiar el formato de las fecha y hora.
--FROM dual; 

---****************************************************************************
--SELECT salary, TO_CHAR(salary,'$99999.00') AS "Salary" --nor permite cambiar el formato de los n�meros, en este caso, poder agregar el signo de pesos
--FROM employees;                                        -- debemos tener cuidado con �sto, ya que la cantidad de 9's debe ser igual al n�mero de numeros de la cantidad m�s grande.


--SELECT TO_DATE('Julio312004', 'fxMonthDDYYYY') AS "Date"   --convierte la fecha de tipo caracter, a tipo fecha.
--SELECT TO_DATE('Noviembre 3, 2001', 'Month dd, yyyy')
--FROM DUAL;  

--SELECT TO_DATE('Julio312004', 'fxMonthDDYYYY') AS "Date"
--FROM DUAL;


---****************************************************************************

--SELECT department_id, last_name, NVL(commission_pct, 0) -- NVL le asigna un valor a los campos que est�n en NULL, este valor debe ser del mismo tipo que est� definido el campo. 
--FROM employees                                            --Esto tambi�n nos permite hacer calculos son tener valores nulos.
--WHERE department_id IN(80,90);

---****************************************************************************

--SELECT last_name, salary, NVL2(commission_pct, salary + (salary * commission_pct), salary) --Es para sustituir valores nulos
--AS income
--FROM employees
--WHERE department_id IN(80,90);


---****************************************************************************
--SELECT last_name, CASE department_id
--WHEN 90 THEN 'Management'
--WHEN 80 THEN 'Sales'
--WHEN 60 THEN 'It'
--ELSE 'Other dept.'END AS "Department" FROM employees;  -- sustituye el n�mero de departament_id de acuerdo al n�mero que tiene.


--SELECT last_name, DECODE(department_id,
--90, 'Management',
--80, 'Sales',
--60, 'It',
--'Other dept.') AS "Department" FROM employees;  --Hace lo mismo que la sentencia pasada, solo que con diferente syntaxis.


---****************************************************************************

SELECT first_name, last_name, job_id, job_title
FROM employees NATURAL JOIN jobs;
--WHERE department_id > 80;         --Une tablas con una o m�s columnas en com�n, y de ah� poder unir m�s campos diferentes entre tablas.

--SELECT last_name, department_name
--FROM employees CROSS JOIN departments;           --Realiza todas las posibles combinaciones entre dos tablas, es demasiado extensa.

SELECT first_name, last_name, manager_id, location_id
FROM employees JOIN departments USING(manager_id);

SELECT first_name, last_name, department_id
FROM employees;

SELECT department_id, department_name
FROM departments;

--********************************************************************************************


SELECT last_name, job_title
FROM employees e JOIN jobs j
ON (e.job_id = j.job_id);


SELECT last_name, department_name AS "Department", city
FROM employees JOIN departments USING (department_id)
JOIN locations USING (location_id);                        --Est� uniendo tres tablas con columnas en com�n.



--****************************************************************************************








--SELECT AVG(salary) FROM employees
--WHERE department_id= 90;