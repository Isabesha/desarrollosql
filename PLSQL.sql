/*
BEGIN 
    FOR c IN (SELECT * FROM USER_TABLES) LOOP
        EXECUTE IMMEDIATE 'CREATE TABLE BECAPLSQL.'||c.TABLE_NAME||' AS SELECT * FROM HR.'||c.TABLE_NAME;
    END LOOP;
END;                         
--Este c�digo es para copiar todas las tablas de otra conecci�n

********************************************************************************
--PRIMER BLOQUE ANONIMO

DECLARE
    v_first_name  VARCHAR2(25);
    v_last_name   VARCHAR2(25);
BEGIN
    SELECT
        first_name,
        last_name
    INTO
        v_first_name,
        v_last_name
    FROM
        employees
    WHERE
        last_name = 'Isabel';

    dbms_output.put_line('The employee of the month is: '
                         || v_first_name
                         || ' '
                         || v_last_name
                         || '.');

EXCEPTION

     WHEN NO_DATA_FOUND THEN  --Excepci�n por si no encuentra ninguno empleado con ese nombre
        dbms_output.put_line('El nombre del usuario que ha solocitado no se encontr� en la BD. POR FAVOR DE VERIFICAR');
    WHEN too_many_rows THEN  --Excepci�n por si hay varios empleados con el mismo nombre.
        dbms_output.put_line('El nombre del usuario que ha solocitado, est� repetido m�s de una vez. POR FAVOR DE VERIFICAR');
END;

--Este es un bloque anonimo, ya que no tiene un nombre, no est� almacenado en la base de datos y solo es compilado cada vez que se ejecuta.
--Lo que hace este bloque, es que declara las variables  v_first_name - v_last_name en la que posteriormente almacenara una copia de los datos de la tabla employees de las columnas first_mane - last_name que cumplan con la condici�n WHERE...
--imprime dichos datos obtenidos, pero en caso de generarse una excepci�n como el que haya varios empleados con el mismo nombre o que no exita ninguno con ese nombre, desplega en pantalla los textos correspondientes. 



--************************************************************************************************************************************************************

--SUBPROGRAMAS: se almacenan en la base de datos y se pueden invocar cada vez que uno lo desee. Existen dos tipos de subprogramas:
--Procedimientos: realizan una acci�n
--Funciones: realizan un calculo, y regresan un valor.

CREATE OR REPLACE PROCEDURE print_date IS
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate, 'Mon DD, YYYY')
    INTO v_date                               --Aqu� se declara una PROCEDIMIENTO, que obtiene la fecha actual y la despliega con cierto formato. Dicho procedimiento ya fue almacenado para ser utilizado posteriormente.
        FROM
        dual;

    dbms_output.put_line(v_date);
END;


--************
BEGIN
    print_date;      --Aqu� se hace uso del procedimiento anterior.
END;


--***************

CREATE OR REPLACE FUNCTION tomorrow (p_today IN DATE) RETURN DATE IS
    v_tomorrow DATE;            --Ahora este es un ejemplo de una fFUNCI�N, que recibir� una fecha a la que le sumar� 1 d�a, y regresar� el d�a siguiente de la fecha solicitada. 
BEGIN
    SELECT
        p_today + 1
    INTO v_tomorrow
    FROM
        dual;

    RETURN v_tomorrow;
END;
--****************************
--primera opci�n
SELECT TOMORROW(SYSDATE) AS "Tomorrow's Date"
FROM DUAL;
--segunda opci�n
BEGIN
DBMS_OUTPUT.PUT_LINE(TOMORROW(SYSDATE));
END;
-- Aqu� tenemos dos formas de hacer uso de la funci�n anterior.




DECLARE
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate)
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;
  
--***********************
  
CREATE FUNCTION num_characters (p_string IN VARCHAR2)
RETURN INTEGER IS
v_num_characters INTEGER;
BEGIN
SELECT LENGTH(p_string) INTO v_num_characters
FROM DUAL;
RETURN v_num_characters;
END;

--Funci�n que recibe una cadena y calcula el n�mero de caracteres y regresa ese valor.

--******************

DECLARE
    v_length_of_string INTEGER;
    var_1 NUMBER(10.6)
BEGIN
    v_length_of_string := num_characters('OracleCorporation');
    DBMS_OUTPUT.PUT_LINE(v_length_of_string);
    v_length_of_string := num_characters(var_1');
    DBMS_OUTPUT.PUT_LINE(v_length_of_string);
END;
 --Se hace uso de la funci�n anterior y calcula el n�mero de caracteres de la palabra y en el caso de la variable var_1, aunque es tipo n�mero, con la funci�n regresa en numero de cifras tomando en cuenta el punto

 ***********************************************************************

DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20) := 'Mike';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
    END;

    dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
END;
--Dentro de los bloques, se pueden anidar m�s bloques; los bloques internos pueden acceder a las variables de los bloques externos, pero no visceversa.
******************************************************************************

DECLARE
    v_first_name  VARCHAR2(20);
    v_last_name   VARCHAR2(20);
BEGIN
    BEGIN
        v_first_name := 'Carmen';
        v_last_name := 'Miranda';
        dbms_output.put_line(v_first_name || ' ' || v_last_name);
    END;

    dbms_output.put_line(v_first_name || ' ' || v_last_name);
END;
--Aqu� el bloque interno, tiene acceso a las variables externas sin problemas

*****************************************************************************

DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name     VARCHAR2(20) := 'Mike';
        v_date_of_birth  DATE := '12-Dec-2002';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
    END;

    dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
END;
--Cuando se anidan bloques, las variables de estos pueden tener el mismo nombre, pero al momento de ejecutarse el codigo, cada bloque hara uso de sus respectivas las varaibles.
--*********************************************************


<<primero>>  --esta es una etiqueta que le podemos dar el nombre que queramos, esto para poder identificar cada uno de los bloques.
DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Abril-1972';
BEGIN
<<segundo>>
    DECLARE
        v_child_name     VARCHAR2(20) := 'Mike';
        v_date_of_birth  DATE := '12-Diciembre-2002';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || primero.v_date_of_birth);  --y as� poder hacer uso de las variables aunque tengan el mismo nombre pero poder especificar el bloque da la variable que se necesita.
        dbms_output.put_line('Child''s Name: ' || v_child_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
    END;
END;


--*********************************************************************************************

--CURSOR EXPLICITO 
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name,
        salary
    FROM
        employees
    WHERE
        department_id = 30;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
    v_sal    employees.salary%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname,
            v_sal;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname);
    END LOOP;

    CLOSE cur_emps;
END;

--******************************************


DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 10;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname;
            EXIT WHEN cur_emps%NOTFOUND; --Aqu� est� la condici�n faltante para que funcione perfectamente
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname);
    END LOOP;
    CLOSE cur_emps;
END;

--En este ejemplo con cursor marca error ya que no hay condici�n para salir del LOOP, se queda en un bucle.



--******************************************************************************
DECLARE
    CURSOR cur_emps IS
    SELECT
        *
    FROM
        employees
    WHERE
        department_id = 30;

    v_emp_record cur_emps%rowtype;  --Aqu� solo se utiliza una sola variable para almacenar todas las columnas de la tabla employees, y no declar variable por variable.
                                    --Esa variable obtiene el tipo de los datos que la tabla employees
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' - '
                             || v_emp_record.last_name);
    END LOOP;

    CLOSE cur_emps;
END;
--******************************************************************************

DECLARE
    CURSOR cur_emps_dept IS
    SELECT
        first_name,
        last_name,
        department_name
    FROM
        employees    e,
        departments  d
    WHERE
        e.department_id = d.department_id;

    v_emp_dept_record cur_emps_dept%rowtype;
BEGIN
    OPEN cur_emps_dept;
    LOOP
        FETCH cur_emps_dept INTO v_emp_dept_record;
        EXIT WHEN cur_emps_dept%notfound;
        dbms_output.put_line(v_emp_dept_record.first_name
                             || ' � '
                             || v_emp_dept_record.last_name
                             || ' � '
                             || v_emp_dept_record.department_name);

    END LOOP;

    CLOSE cur_emps_dept;
END;

--*************************************************************************
FALTA PONER AQU� LA DESCRIPCI�N

DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees;

    v_emp_record cur_emps%rowtype;
     v_rowcount NUMBER;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%ROWCOUNT > 10 OR cur_emps%NOTFOUND;
        v_rowcount := cur_emps%ROWCOUNT;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name || ' ' || v_rowcount );
    END LOOP;
    v_rowcount := cur_emps%ROWCOUNT;
    dbms_output.put_line(v_rowcount );
    CLOSE cur_emps;
END;



--8.2

CREATE OR REPLACE PROCEDURE raise_salary (
    p_id       IN  employees.employee_id%TYPE,
    p_percent  IN  NUMBER
) IS
BEGIN
    UPDATE employees
    SET
        salary = salary * ( 1 + p_percent / 100 )
    WHERE
        employee_id = p_id;

END raise_salary;

--**************************************************************************
--Aqu� se declara otro procedimiento que hace uso del procedimiento declarado anteriormente

CREATE OR REPLACE PROCEDURE process_employees IS
    CURSOR emp_cursor IS
    SELECT
        employee_id
    FROM
        employees;

BEGIN
    FOR v_emp_rec IN emp_cursor LOOP
        raise_salary(v_emp_rec.employee_id, 10);
    END LOOP;
END process_employees;
--*************************************************************************

CREATE OR REPLACE PROCEDURE query_emp (p_id      IN   employees.employee_id%TYPE, p_name    OUT  employees.last_name%TYPE, p_salary  OUT  employees.salary%TYPE) IS
BEGIN
    SELECT
        last_name,
        salary
    INTO
        p_name,
        p_salary
    FROM
        employees
    WHERE
        employee_id = p_id;

END query_emp;

--**********************************************************************************
DECLARE
    a_emp_name  employees.last_name%TYPE;
    a_emp_sal   employees.salary%TYPE;
BEGIN
    query_emp(178, a_emp_name, a_emp_sal);
    dbms_output.put_line('Name: ' || a_emp_name);
    dbms_output.put_line('Salary: ' || a_emp_sal);
END;

--*****************************************************************************

CREATE OR REPLACE PROCEDURE format_phone (
    p_phone_no IN OUT VARCHAR2
) IS
BEGIN
    p_phone_no := '('
                  || substr(p_phone_no, 1, 3)
                  || ')'
                  || substr(p_phone_no, 4, 3)
                  || '-'
                  || substr(p_phone_no, 7);
END format_phone;

--*********************************************************************

--Aqu� se hace 
DECLARE
    a_phone_no VARCHAR2(13);
BEGIN
    a_phone_no := '8006330575';
    format_phone(a_phone_no);
    dbms_output.put_line('The formatted number is: ' || a_phone_no);
END;

--**************************************************************************


--FUNCIONES 
CREATE OR REPLACE FUNCTION get_sal (
    p_id IN employees.employee_id%TYPE
) RETURN NUMBER IS
    v_sal employees.salary%TYPE := 0;
BEGIN
    SELECT
        salary
    INTO v_sal
    FROM
        employees
    WHERE
        employee_id = p_id;

    RETURN v_sal;
END get_sal;

--******************************************

CREATE OR REPLACE FUNCTION get_sal (p_id IN employees.employee_id%TYPE) 
RETURN NUMBER IS
    v_sal employees.salary%TYPE := 0;
BEGIN
    SELECT
        salary
    INTO v_sal
    FROM
        employees
    WHERE
        employee_id = p_id;

    RETURN v_sal;
EXCEPTION
    WHEN no_data_found THEN
        RETURN NULL;
END get_sal;

--********************************************************************
--Hace uso de la funci�n declarada anteriormente
DECLARE
    v_sal employees.salary%TYPE;
BEGIN v_sal := get_sal(100); 
    DBMS_OUTPUT.put_line (v_sal);
END;

--Una funci�n que regresa un valor tipo BOOLEAN, y no puede ser usada en un SELECT pero si en otros bloques.
CREATE OR REPLACE FUNCTION valid_dept (p_dept_no departments.department_id%TYPE) 
RETURN BOOLEAN IS
    v_valid VARCHAR2(1);
BEGIN
    SELECT
        'x'
    INTO v_valid
    FROM
        departments
    WHERE
        department_id = p_dept_no;

    return(true);
EXCEPTION
    WHEN no_data_found THEN
        return(false);
    WHEN OTHERS THEN
        NULL;
END;

--***********************************************************************

DECLARE
v_departmentid NUMBER;

BEGIN
v_departmentid := 100;
IF valid_dept(v_departmentid) THEN
    DBMS_OUTPUT.put_line ('S� existe el depto');
-- this was a valid department, so we�ll do this part of the code, e.g. an insert into employees
ELSE
    DBMS_OUTPUT.put_line ('NO existe el depto');
-- valid_dept returned a false, so we are not doing the insert
END IF;
END;

--*********************************************

--utiliza el IN
CREATE OR REPLACE FUNCTION tax (p_value IN NUMBER) RETURN NUMBER IS
BEGIN
    return(p_value * 0.08);
END tax;

--******************************************************
SELECT employee_id, last_name, salary, tax(p_value => salary)  --Se puede utiizar el simbolo => para determinar el valor al parametro, esto porque es una funcion que nosotros creamos, pero en 
--las funciones del systema no se puede utilizar.
FROM employees
WHERE department_id = 50;

--Funciones del sistema no pueden ser transacciones 

--*********************************************************************

SELECT *
FROM employees
WHERE department_id = 80;

--***************************************************************************

SELECT object_type, object_name FROM USER_OBJECTS; --Diccionario de datos 

SELECT object_type, COUNT(*) FROM USER_OBJECTS
GROUP BY object_type;


SELECT *
FROM employees
WHERE manager_id IN (99, 100, 101);

--*********************************************************

CREATE OR REPLACE PROCEDURE add_department_noex (
    p_name  VARCHAR2,
    p_mgr   NUMBER,
    p_loc   NUMBER
) IS
BEGIN
    INSERT INTO departments (
        department_id,
        department_name,
        manager_id,
        location_id
    ) VALUES (
        departments_seq.NEXTVAL,
        p_name,
        p_mgr,
        p_loc
    );

    dbms_output.put_line('Added Dept: ' || p_name);
END;

--**********************************************************
BEGIN
add_department_noex('Media', 100, 1800);
add_department_noex('Editing', 99, 1800);
add_department_noex('Advertising', 101, 1800);
END;

SELECT *
FROM departments
WHERE manager_id IN (99, 100, 101);

--***********************************************************

CREATE OR REPLACE PACKAGE global_consts IS
mile_to_kilo CONSTANT NUMBER := 1.6093;
kilo_to_mile CONSTANT NUMBER := 0.6214;
yard_to_meter CONSTANT NUMBER := 0.9144;
meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;

GRANT EXECUTE ON global_consts TO PUBLIC;  --permiso del paquete anterior para que pueda ser p�blico, y utilizado sin problemas

--**********************************************************
DECLARE
distance_in_miles NUMBER(5) := 5000;
distance_in_kilo NUMBER(6,2);
BEGIN
distance_in_kilo :=
distance_in_miles * global_consts.mile_to_kilo;
DBMS_OUTPUT.PUT_LINE(distance_in_kilo);
END;


--**********************************************************************

CREATE OR REPLACE PACKAGE our_exceptions IS
    e_cons_violation EXCEPTION;
    PRAGMA exception_init ( e_cons_violation, -2292 );
    e_value_too_large EXCEPTION;
    PRAGMA exception_init ( e_value_too_large, -1438 );
END our_exceptions;

GRANT EXECUTE ON our_exceptions TO PUBLIC;

CREATE TABLE excep_test (
    number_col NUMBER(3)
); --Crea una tabla llamada 

BEGIN
    INSERT INTO excep_test ( number_col ) VALUES ( 999 );

EXCEPTION
    WHEN our_exceptions.e_value_too_large THEN
        dbms_output.put_line('Value too big for column datatype');
END;

SELECT
    *
FROM
    excep_test;

--**************************************************************************
CREATE OR REPLACE PACKAGE taxes_pkg IS
    FUNCTION tax (
        p_value IN NUMBER
    ) RETURN NUMBER;

END taxes_pkg;

CREATE OR REPLACE PACKAGE BODY taxes_pkg IS

    FUNCTION tax (
        p_value IN NUMBER
    ) RETURN NUMBER IS
        v_rate NUMBER := 0.08;
    BEGIN
        return(p_value * v_rate);
    END tax;

END taxes_pkg;

SELECT
    taxes_pkg.tax(salary),
    salary,
    last_name
FROM
    employees;

--**********************************************************************

CREATE OR REPLACE PROCEDURE sel_one_emp (
    p_emp_id  IN   employees.employee_id%TYPE,
    p_emprec  OUT  employees%rowtype
) IS BEGIN
    SELECT
        *
    INTO p_emprec
    FROM
        employees
    WHERE
        employee_id = p_emp_id;
EXCEPTION WHEN no_data_found THEN
     dbms_output.put_line('DATO NO ENCONTRADO');
END sel_one_emp;


DECLARE
v_emprec employees%ROWTYPE;
BEGIN
sel_one_emp(100, v_emprec);

dbms_output.put_line(v_emprec.last_name);

END;

--***************************************************************************

CREATE OR REPLACE PACKAGE curs_pkg IS
    CURSOR emp_curs IS
    SELECT
        employee_id
    FROM
        employees
    ORDER BY
        employee_id;

    PROCEDURE open_curs;

    FUNCTION fetch_n_rows (
        n NUMBER := 1
    ) RETURN BOOLEAN;

    PROCEDURE close_curs;

END curs_pkg;

--*********************************************************
CREATE OR REPLACE PACKAGE BODY curs_pkg IS

    PROCEDURE open_curs IS
    BEGIN
        IF NOT emp_curs%isopen THEN
            OPEN emp_curs;
        END IF;
    END open_curs;

    FUNCTION fetch_n_rows (
        n NUMBER := 1
    ) RETURN BOOLEAN IS
        emp_id employees.employee_id%TYPE;
    BEGIN
        FOR count IN 1..n LOOP
            FETCH emp_curs INTO emp_id;
            EXIT WHEN emp_curs%notfound;
            dbms_output.put_line('Id: ' ||(emp_id));
        END LOOP;

        RETURN emp_curs%found;
    END fetch_n_rows;

    PROCEDURE close_curs IS
    BEGIN
        IF emp_curs%isopen THEN
            CLOSE emp_curs;
        END IF;
    END close_curs;

END curs_pkg;

--*********************************************************************

DECLARE
    v_more_rows_exist BOOLEAN := true;
BEGIN
    curs_pkg.open_curs; --1
    LOOP
        v_more_rows_exist := curs_pkg.fetch_n_rows(3); --2
        dbms_output.put_line('-------');
        EXIT WHEN NOT v_more_rows_exist;
    END LOOP;

    curs_pkg.close_curs; --3
END;



--******************************************************************************


CREATE OR REPLACE PROCEDURE insert_emps IS
    TYPE t_emps IS
        TABLE OF employees%rowtype INDEX BY BINARY_INTEGER;
    v_emptab t_emps;
BEGIN
    SELECT
        *
    BULK COLLECT
    INTO v_emptab
    FROM
        employees;

    FORALL i IN v_emptab.first..v_emptab.last
        INSERT INTO emp VALUES v_emptab ( i );

    FOR i IN v_emptab.first..v_emptab.last LOOP
        dbms_output.put_line('Inserted: '
                             || i
                             || ' '
                             || SQL%bulk_rowcount(i)
                             || 'rows');
    END LOOP;

END insert_emps;


--CREAR UNA TABLA COPIA DE EMPLOYEES Y DESPU�S BORRAR SU CONTENIDO Y AGREGARLA EN LUGAR DE "emp"

SELECT
    *
FROM
    employees;

--****************************************************************************************


CREATE OR REPLACE TRIGGER checar_salario BEFORE
    UPDATE OF salary ON employees
    FOR EACH ROW
DECLARE v_sal_min NUMBER;

BEGIN
        
        v_sal_min:= 550;

    IF :new.salary < v_sal_min OR :new.salary < :old.salary THEN
        raise_application_error(-20508, 'No disminuir salario.');
    END IF;

END;


--*******************************************************************
UPDATE employees
SET salary = 4; 

*/

CREATE OR REPLACE PROCEDURE query_country (
    p_tag   IN   countries.country_id%TYPE,
    p_name  OUT  countries.country_name%TYPE,
    p_id    OUT  countries.region_id%TYPE
) IS
BEGIN
    SELECT
        country_name,
        region_id
    INTO
        p_name,
        p_id
    FROM
        countries
    WHERE
        country_id = p_tag;

END query_country;


DECLARE
    a_emp_name  countries.country_name%TYPE;
    a_emp_id    countries.region_id%TYPE;
BEGIN
    query_country('MX', a_emp_name, a_emp_id);
    dbms_output.put_line('Country Name: ' || a_emp_name);
    dbms_output.put_line('Region ID: ' || a_emp_id);
END;



DECLARE
    e_myexcep EXCEPTION;
    e_myexcep2 EXCEPTION;
BEGIN
    BEGIN
        BEGIN
            RAISE e_myexcep;
            dbms_output.put_line('Message 1 ');
        END;
    EXCEPTION
        WHEN e_myexcep THEN
            dbms_output.put_line('Message 2');             RAISE e_myexcep2;
            dbms_output.put_line('Message 3');
    END;
EXCEPTION
    WHEN e_myexcep2 THEN
        dbms_output.put_line('Message 4');
END; 