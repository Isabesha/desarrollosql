--SELECT last_name, (SYSDATE - hire_date) / 7  -- DA EL NUMERO DE SEMANAS QUE HA TRABJADO DESDE QUE FUE CONTRATADO
--FROM employees;


--******************************************************************************

--SELECT employee_id, (end_date - start_date)/365 AS "Tenure in last job" FROM job_history;

---*****************************************************************************
--SELECT last_name, hire_date FROM employees
--WHERE MONTHS_BETWEEN (SYSDATE, hire_date) > 50;  --TE DA LOS MESES QUE HAY ENTRE UNA FECHA Y OTRA Y MUESTRA LOS QUE SON MAYORES A 50

---*****************************************************************************
--SELECT ADD_MONTHS (SYSDATE, 12) AS "Next Year" FROM dual;  
--AGREGA CIERTO NUMERO DE MESES A LA FECHA ACTUAL

---*****************************************************************************

--SELECT NEXT_DAY (SYSDATE, 'S�bado') AS "Next Saturday" FROM dual; --Te da la fecha del d�a proximo que se se�ala

---*****************************************************************************

--SELECT LAST_DAY (SYSDATE) AS "End of the Month" FROM dual; Da el �ltimo d�a del mes de la fecha actual


---****************************************************************************

--SELECT hire_date, ROUND(hire_date, 'Month') FROM employees
--WHERE department_id = 50;      --Con el ROUND en este caso, lo que hace es que si la fecha est� dentro de los primeros 15 d�as, redondea la fecha al primer d�a de ese mismo mes, 
                               --De lo contrario, pasando los 15 d�as, redondea la fecha al primer d�a del SIGUIENTE mes.
                               --Lo mismo hace con YEAR, estando dentro de los primeros 6 meses, la fecha la pasa para el primer d�a del primer mes de ese a�o, de lo contrario, pasa la fecha al primer d�a del primer mes del siguiente a�o.
---****************************************************************************

--SELECT hire_date, TRUNC(hire_date, 'Month') FROM employees
--WHERE department_id = 50;     --TRUNC lo que hace es (en 'Month') regresa la fecha con el primer d�a del mes de la fecha original, en el caso de 'Year' regresa la fecha del primer d�a del primer mes del a�o de la fecha original.

---****************************************************************************

--SELECT employee_id, hire_date,
--ROUND(MONTHS_BETWEEN(SYSDATE, hire_date)) AS TENURE,  --Cuenta lo meses ue hay entre la fecha actual y la fecha de contrataci�n, y ese n�mero lo redondea.
--ADD_MONTHS (hire_date, 6) AS REVIEW, -- Agrega 6 meses a la fecha de contrataci�n
--NEXT_DAY(hire_date, 'VIERNES'), LAST_DAY(hire_date)  --Da la fecha de viernes proximo y la fecha del �ltimo mes a la fecha de contrataci�n.
--FROM employees
--WHERE MONTHS_BETWEEN (SYSDATE, hire_date) > 36; --unicamente mostrar� los empleados que la diferencia de meses entre la fecha de contrataci�n y la fecha actual sea mayor a 36


---****************************************************************************
--SELECT TO_CHAR(hire_date, 'fmDay ddthsp Mon, YYYY') --cambia el formato de la fecha y la pasa a tipo char.
--FROM employees;


SELECT TO_CHAR(SYSDATE, 'hh:mm:ss pm')
FROM dual;



--SELECT AVG(salary) FROM employees
--WHERE department_id= 90;